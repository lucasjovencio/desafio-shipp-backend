<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['search.log'] ], function () {
    Route::get('/stores/{latitude}/{longitude}','StoreController@stores');
});
Route::get('/logs','LogController@list');
