# Lucas Coutinho Jovencio

Teste tecnico para desenvolvedor backend, desenvolvido com as tecnologias:

  - PHP 7.3.9
  - Laravel/framework ^6.2
  - SQLite   

# Instalação expressa

  - touch storage/app/db.sqlite && composer install && php artisan dev:install && ./vendor/bin/phpunit
  - php artisan serve

# Instalação manual
```sh
$ touch storage/app/db.sqlite
$ composer install
$ cp .env.example .env
$ php artisan key:generate
$ php artisan config:cache
$ php artisan migrate:fresh --seed
$ ./vendor/bin/phpunit
$ php artisan serve
```

# Curl stores
```sh
$ curl --request GET \
  --url http://127.0.0.1:8000/api/V1/stores/42.69743/-73.809819 \
  --header 'v1-authentication: base64:zS/cYzq+j7lOfdmn5rV0kcRnIIg06aomJVwmDW1i51o='
```

# Curl logs
```sh
$ curl --request GET \
  --url http://127.0.0.1:8000/api/V1/logs \
  --header 'v1-authentication: base64:zS/cYzq+j7lOfdmn5rV0kcRnIIg06aomJVwmDW1i51o='
```

# License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

