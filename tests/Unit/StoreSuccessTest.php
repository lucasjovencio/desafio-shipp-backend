<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class StoreSuccessTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->withHeaders([
            'v1-authentication' => 'base64:zS/cYzq+j7lOfdmn5rV0kcRnIIg06aomJVwmDW1i51o=',
        ])->json('GET', '/api/V1/stores/42.69743/-73.809819');
        $response->assertStatus(200);
    }
}
