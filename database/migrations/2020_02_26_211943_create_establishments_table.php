<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablishmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establishments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('license_number',15);
            $table->string('operation_type',20);
            $table->string('establishiment_type',20);
            $table->string('entity_name',200);
            $table->string('dba_name',200);
            $table->integer('square_footage');
            $table->string('address_line_2',200)->nullable();
            $table->string('address_line_3',200)->nullable();
            $table->bigInteger('addresses_id')->unsigned()->index();
            $table->foreign('addresses_id')->references('id')->on('addresses');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('establishments');
    }
}
