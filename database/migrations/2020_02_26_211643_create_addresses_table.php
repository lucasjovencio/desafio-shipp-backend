<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('street_number',50);
            $table->json('location');
            $table->bigInteger('zip_codes_id')->unsigned()->index();
            $table->foreign('zip_codes_id')->references('id')->on('zip_codes');
            $table->bigInteger('streets_id')->unsigned()->index();
            $table->foreign('streets_id')->references('id')->on('streets');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
