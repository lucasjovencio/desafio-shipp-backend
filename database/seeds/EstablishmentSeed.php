<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

use App\Services\Establishment\CreateEstablishmentService;
use App\Services\Address\CreateAddressService;
use Illuminate\Support\Facades\DB;
use Eastwest\Json\Json;

class EstablishmentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(
        CreateEstablishmentService $createEstablishment,
        CreateAddressService $createAddress
    )
    {
        $this->command->line('Iniciando importação, pode levar algum tempo...');
        $this->command->getOutput()->progressStart(29389);
        $handle = fopen("database/seeds/csv/stores.csv", "r");
        $header = true;
        try{
            DB::beginTransaction();
            while ($csvLine = fgetcsv($handle, 1000, ",")) {
                if ($header) {
                    $header = false;
                } else {
                    $location = $csvLine[14];
                    $location = str_replace("'",'"',$location);
                    $location = str_replace('"{','{',$location);
                    $location = str_replace('}"','}',$location);
                    $location = str_replace("False","false",$location);

                    $address = $createAddress->generate(
                        (Object)[
                            'country' => trim($csvLine[0]),
                            'state' => trim($csvLine[11]),
                            'city' => trim($csvLine[10]),
                            'zip_code' => trim($csvLine[12]),
                            'street' => trim($csvLine[7]),
                            'street_number' => trim($csvLine[6]),
                            'location' => ($location) ? $location : '{}'
                        ]
                    )->getAddress();
    
                    $establishment = $createEstablishment->generate(
                        (Object)[
                            'license_number' => trim($csvLine[1]), 
                            'operation_type' => trim($csvLine[2]), 
                            'establishiment_type' => trim($csvLine[3]),
                            'entity_name' => trim($csvLine[4]),
                            'dba_name' => trim($csvLine[5]),
                            'square_footage' => trim($csvLine[13]),
                            'address_line_2' => trim($csvLine[8]),
                            'address_line_3' => trim($csvLine[9]),
                            'addresses_id' => $address->id
                        ]
                    );
                    $this->command->getOutput()->progressAdvance();
                }
            }
            $this->command->getOutput()->progressFinish();
            DB::commit();
            $this->command->line('Importação concluida com sucesso!');
        }catch(Illuminate\Database\QueryException $e){
            DB::rollBack();
            dd($e);
        }
        catch(\Exception $e)
        {
            DB::rollBack();
            dd($e);
        }
    }
}
