<?php

namespace App\Interfaces;

interface DefaultRepositoryInterfaces
{   

    public function create(array $data);

    public function find(int $id);

    public function getAll();

    public function firstOrCreate(array $data);

    public function get();

    public function count();
    
}