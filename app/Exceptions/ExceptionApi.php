<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class ExceptionApi extends Exception
{
    protected $type;

    public function __construct(array $data = ['message' => '', 'type' => ''], $code = 0, Throwable $previous = null)
    {
        $this->setType($data['type']);
        parent::__construct($data['message'], $code, $previous);
    }

    public function getResponse()
    {
        return [
            'message' => $this->getMessage(),
            'type' => $this->getType()
        ];
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): void
    {
        $this->type = $type;
    }

}

