<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class InstallDev extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->line('Iniciando configuração do projeto...');
        $output = shell_exec('touch storage/app/db.sqlite'); 
        $output = shell_exec('cp .env.example .env'); 
        $output = shell_exec('php artisan key:generate'); 
        $output = shell_exec('php artisan config:cache'); 
        $this->info("{$output}");
        $this->line('Rodando Migrate & Seed...');
        $output = shell_exec('php artisan migrate:fresh --seed'); 
        $this->info("{$output}");
        $this->info("Run ./vendor/bin/phpunit");
    }
}
