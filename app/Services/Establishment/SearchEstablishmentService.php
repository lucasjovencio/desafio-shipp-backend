<?php

namespace App\Services\Establishment;

use App\Repositories\EstablishmentRepository;
class SearchEstablishmentService 
{
    private $stablishment;
    private $establishments;
    public function __construct(
        EstablishmentRepository $establishment
    )
    {
        $this->establishment = $establishment;
    }


    public function searchDistanceRadius($latitude,$logintude)
    {
        $this->establishment = $this->establishment->nearbySqlite($latitude,$logintude);
        return $this;
    }

    public function withDistance($latitude,$logintude)
    {
        $this->establishment = $this->establishment->withDistance($latitude,$logintude);
        return $this;
    }

    public function get()
    {
        $this->establishments =  $this->establishment->get();
        return $this;
    }

    public function orderByDistance()
    {
        $this->establishments = $this->establishments->sortBy('distance');
        return $this;
    }

    public function getEstablishments()
    {
        return $this->establishments;
    }

    public function count()
    {
        return $this->establishments->count();
    }
}