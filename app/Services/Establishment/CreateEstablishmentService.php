<?php

namespace App\Services\Establishment;

use App\Repositories\EstablishmentRepository;
class CreateEstablishmentService 
{
    private $stablishment;
    private $stablishment_repo;
    public function __construct(
        EstablishmentRepository $establishment,
        EstablishmentRepository $establishment_repo
    )
    {
        $this->establishment = $establishment;
        $this->establishment_repo = $establishment_repo;
    }

    public function generate(Object $data)
    {
        $this->establishment = $this->establishment_repo->create((array)$data);
        return $this;
    }

    public function getEstablishment()
    {
        return $this->establishment;
    }
}