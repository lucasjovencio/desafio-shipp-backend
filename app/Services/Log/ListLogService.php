<?php

namespace App\Services\Log;

use App\Repositories\LogRepository;
class ListLogService 
{
    private $log;
    public function __construct(
        LogRepository $log
    )
    {
        $this->log = $log;
    }

    public function get()
    {
        return $this->log->get();
    }

}