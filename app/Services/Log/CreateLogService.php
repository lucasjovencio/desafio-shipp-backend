<?php

namespace App\Services\Log;

use App\Repositories\LogRepository;
class CreateLogService 
{
    private $log;
    private $log_repo;
    public function __construct(
        LogRepository $log,
        LogRepository $log_repo
    )
    {
        $this->log = $log;
        $this->log_repo = $log_repo;
    }

    public function generate(Object $data)
    {
        $this->log = $this->log_repo->create((array)$data);
        return $this;
    }

}