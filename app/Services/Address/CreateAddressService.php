<?php

namespace App\Services\Address;

use App\Repositories\AddressRepository;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\ZipCode;
use App\Models\Street;

class CreateAddressService 
{
    private $address;
    private $address_repo;
    public function __construct(
        AddressRepository $address,
        AddressRepository $address_repo
    )
    {
        $this->address = $address;
        $this->address_repo = $address_repo;
    }

    public function generate(Object $data)
    {
        $this->address = $this->address_repo->create(
            array_merge(['street_number'=>$data->street_number,'location'=>$data->location],$this->firstOrCreateZipCode($data),$this->firstOrCreateStreet($data))
        );
        return $this;
    }

    private function firstOrCreateZipCode(Object $data)
    {
        $country    = Country::firstOrCreate(['country' => $data->country]);
        $state      = State::firstOrCreate(['state' => $data->state,'countries_id'=>$country->id]);
        $city       = City::firstOrCreate(['city' => $data->city,'states_id'=>$state->id]);
        $zip_code   = ZipCode::firstOrCreate(['zip_code' => $data->zip_code,'cities_id'=>$city->id]);
        return ['zip_codes_id'=>$zip_code->id];
    }

    private function firstOrCreateStreet(Object $data)
    {
        $street    = Street::firstOrCreate(['street' => $data->street]);
        return ['streets_id'=>$street->id];
    }

    public function getAddress()
    {
        return $this->address;
    }
}