<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (DB::Connection() instanceof \Illuminate\Database\SQLiteConnection) {
            DB::connection()->getPdo()->sqliteCreateFunction('distance', function ($lat1,$lon1,$lat2,$lon2) {
                $lat1 = deg2rad($lat1);
                $lat2 = deg2rad($lat2);
                $lon1 = deg2rad($lon1);
                $lon2 = deg2rad($lon2);
                $number = (6378.1 * acos(cos($lat1) * cos($lat2) * cos($lon2 - $lon1) + sin($lat1) * sin($lat2)));
                return floatval(number_format(strval($number),3,'.',''));
            },4);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
