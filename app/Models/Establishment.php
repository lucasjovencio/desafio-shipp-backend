<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Establishment extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'license_number', 'operation_type', 'establishiment_type','entity_name','dba_name',
        'square_footage','address_line_2','address_line_3','addresses_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'addresses_id','deleted_at','updated_at','created_at'
    ];

    protected $appends = [
        'distance'
    ];

    /* Attributes */

    public function getDistanceAttribute()
    {
        return floatval(number_format(strval($this->address->distance ?? 0),3,'.',''));
    }


    /* Relations */
    public function address()
    {
        return $this->belongsTo('App\Models\Address','addresses_id');
    }

    /* Scopes */

    public function scopeNearbySqlite($query,$latitude,$longitude)
    {
        if($latitude && $longitude) {
            return $query->whereHas('address', function($address) use($latitude,$longitude){
                    return $address->select(DB::raw("*, ( distance(json_extract(location, '$.latitude'),json_extract(location, '$.longitude'),{$latitude},{$longitude})) AS distance
                    "))
                    ->where('location->latitude', '<>',NULL)
                    ->where('location->longitude', '<>',NULL)
                    ->groupBy('addresses.id')
                    ->havingRaw('distance >= 0')
                    ->havingRaw('distance < 6.5')
                    ->orderByRaw('distance desc');
            });
        }
        return $query;
    }

    public function scopeNearby($query,$latitude,$longitude)
    {
        if($latitude && $longitude) {
            return $query->whereHas('address', function($address) use($latitude,$longitude){
                    return $address->select(DB::raw("
                        ( 6367 * 
                                acos( 
                                    cos( radians({$latitude}) ) * 
                                    cos( radians( json_extract(location, '$.latitude') ) ) * 
                                    cos( 
                                        radians( json_extract(location, '$.longitude') ) - 
                                        radians({$longitude}) ) + 
                                        sin( radians({$latitude}) 
                                    ) * 
                                    sin( 
                                        radians( json_extract(location, '$.latitude') ) 
                                    ) 
                                ) 
                        ) AS distance"
                    ))
                    ->where('location->latitude', '<>',NULL)
                    ->where('location->longitude', '<>',NULL)
                    ->having('distance', '<', 6.5)
                    ->having('distance', '>=', 0)
                    ->orderBy('distance','ASC');
            });
        }
        return $query;
    }
}
