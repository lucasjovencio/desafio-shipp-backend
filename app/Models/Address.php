<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Address extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_number','location','zip_codes_id','streets_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'zip_codes_id','streets_id','deleted_at','updated_at','created_at'
    ];


    public function scopeNearbySqlite($query,$latitude,$longitude){
        if($latitude && $longitude) {
            return $query->select(DB::raw("*, ( distance(json_extract(location, '$.latitude'),json_extract(location, '$.longitude'),{$latitude},{$longitude})) AS distance"));
        }
        return $query;
    }

    public function scopeNearby($query,$latitude,$longitude)
    {
        if($latitude && $longitude) {
            return $query->select(DB::raw("*, ( 6367 * acos( cos( radians({$latitude}) ) * cos( radians( json_extract(location, '$.latitude') ) ) * cos( radians( json_extract(location, '$.longitude') ) - radians({$longitude}) ) + sin( radians({$latitude}) ) * sin( radians( json_extract(location, '$.latitude') ) ) ) ) AS distance"));
        }
        return $query;
    }
}
