<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Log extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'latitude', 'longitude', 'status_code','establishments_count'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'addresses_id','deleted_at','updated_at','created_at'
    ];

    protected $appends = [
        'datetime_br'
    ];


    public function getDatetimeBrAttribute()
    {
        return Carbon::parse($this->created_at)->setTimezone('America/Sao_Paulo')->format('Y-m-d H:i:s');
    }

}
