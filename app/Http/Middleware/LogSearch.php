<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\Establishment\SearchEstablishmentService;
use App\Services\Log\CreateLogService;
use App\Exceptions\ExceptionApi;
use Exception;
use App\Traits\JsonResponse;
class LogSearch
{
    use JsonResponse;
    private $searchEstablishment;
    private $createLog;
    public function __construct(
        SearchEstablishmentService $searchEstablishment,
        CreateLogService  $createLog 
    )
    {
        $this->searchEstablishment = $searchEstablishment;
        $this->createLog = $createLog;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     * 
     */
    
    public function handle(
        $request, 
        Closure $next
    )
    {
        try{
            $count = $this->searchEstablishment
                ->searchDistanceRadius($request->route('latitude'),$request->route('longitude'))
                ->get()
                ->count();
            $this->createLog->generate((Object)[
                'latitude' => $request->route('latitude'), 
                'longitude'=> $request->route('longitude'), 
                'status_code'=> 200,
                'establishments_count'=> $count
            ]);
            return $next($request);
        }catch (ExceptionApi $e){
            return $this->JsonResponseError($e->getResponse(),$e->getCode());
        }catch (Exception $e){
            return $this->JsonResponseError($e->getMessage(),500);
        }
    }
}
