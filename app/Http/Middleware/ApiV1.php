<?php

namespace App\Http\Middleware;

use Closure;

class ApiV1
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('v1-authentication') == config('app.key_v1')){
            return $next($request);
        }
        abort(403,'Authentication falied.');
    }
}
