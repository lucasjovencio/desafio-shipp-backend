<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Log\ListLogService;
use App\Exceptions\ExceptionApi;
use Exception;
use App\Traits\JsonResponse;
class LogController extends Controller
{
    use JsonResponse;
    
    public function list(ListLogService $listLog)
    {
        try{
            return $this->JsonResponseSuccess(
                    $listLog->get()
                    ,200);
        } catch (ExceptionApi $e){
            return $this->JsonResponseError($e->getResponse(),$e->getCode());
        }catch (Exception $e){
            return $this->JsonResponseError($e->getMessage(),500);
        }
    }
}
