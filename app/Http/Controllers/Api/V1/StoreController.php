<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Establishment\SearchEstablishmentService;
use App\Exceptions\ExceptionApi;
use Exception;
use App\Traits\JsonResponse;
class StoreController extends Controller
{
    use JsonResponse;
    
    public function stores($latitude,$longitude,SearchEstablishmentService $search)
    {
        try{
            return $this->JsonResponseSuccess(
                    $search
                        ->searchDistanceRadius($latitude,$longitude)
                        ->withDistance($latitude,$longitude)
                        ->get()
                        ->orderByDistance()
                        ->getEstablishments()
                    ,200);
        } catch (ExceptionApi $e){
            return $this->JsonResponseError($e->getResponse(),$e->getCode());
        }catch (Exception $e){
            return $this->JsonResponseError($e->getMessage(),500);
        }
    }
}
