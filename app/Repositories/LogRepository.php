<?php

namespace App\Repositories;

use App\Models\Log as Model;;
use App\Traits\RepositoryTraits;

class LogRepository
{
    use RepositoryTraits;
    private $model;

    public function __construct(Model $log)
    {
        $this->model = $log;
    }

}