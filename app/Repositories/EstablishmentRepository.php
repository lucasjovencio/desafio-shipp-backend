<?php

namespace App\Repositories;

use App\Models\Establishment as Model;;
use App\Traits\RepositoryTraits;

class EstablishmentRepository
{
    use RepositoryTraits;
    private $model;

    public function __construct(Model $stablishment)
    {
        $this->model = $stablishment;
    }
    

    public function nearbySqlite($latitude,$longitude)
    {
        $this->model = $this->model->nearbySqlite($latitude,$longitude);
        return $this;
    }

    public function withDistance($latitude,$longitude)
    {
        $this->model = $this->model->with([
            'address' => function($query) use($latitude,$longitude){
                $query->nearbySqlite($latitude,$longitude);
            }
        ]);
        return $this;
    }
    
}