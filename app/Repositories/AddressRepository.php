<?php

namespace App\Repositories;

use App\Models\Address as Model;;
use App\Traits\RepositoryTraits;

class AddressRepository
{
    use RepositoryTraits;
    private $model;

    public function __construct(Model $address)
    {
        $this->model = $address;
    }

}