<?php

namespace App\Traits;

trait RepositoryTraits
{
    public function find(int $id)
    {
        return $this->model::find($id);
    }   

    public function getAll()
    {
        return $this->model->all();
    }

    public function firstOrCreate(array $data)
    {
        return $this->model::firstOrCreate($data);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function get()
    {
        return $this->model->get();
    }

    public function count()
    {
        return $this->model->count();
    }
    
}